<?php
class gameComponent{
	public function setRandomNo(){
		$setRandomNo = rand(1,10);
		if( $setRandomNo == 1 ){
			return 'score=0&c11=2&c12=0&c13=0&c14=0&c21=2&c22=0&c23=0&c24=0&c31=0&c32=0&c33=2&c34=0&c41=0&c42=0&c43=0&c44=4';
		}
		if( $setRandomNo == 2 ){
			return 'score=0&c11=2&c12=0&c13=0&c14=0&c21=0&c22=2&c23=0&c24=0&c31=0&c32=0&c33=0&c34=0&c41=0&c42=0&c43=0&c44=4';
		}
		if( $setRandomNo == 3 ){
			return 'score=0&c11=0&c12=0&c13=0&c14=0&c21=0&c22=0&c23=0&c24=0&c31=0&c32=0&c33=0&c34=0&c41=0&c42=2&c43=0&c44=4';
		}
		if( $setRandomNo == 4 ){
			return 'score=0&c11=0&c12=0&c13=0&c14=0&c21=0&c22=2&c23=4&c24=0&c31=0&c32=0&c33=0&c34=0&c41=0&c42=0&c43=0&c44=0' ;
		}
		if( $setRandomNo == 5 ){
			return 'score=0&c11=0&c12=0&c13=0&c14=2&c21=0&c22=0&c23=0&c24=0&c31=0&c32=0&c33=4&c34=0&c41=0&c42=0&c43=0&c44=0' ;
		}
		if( $setRandomNo == 6 ){
			return 'score=0&c11=0&c12=0&c13=2&c14=0&c21=0&c22=0&c23=2&c24=0&c31=0&c32=0&c33=0&c34=0&c41=0&c42=0&c43=0&c44=0';
		}
		if( $setRandomNo == 7 ){
			return 'score=0&c11=0&c12=0&c13=0&c14=0&c21=0&c22=0&c23=4&c24=0&c31=0&c32=0&c33=0&c34=0&c41=0&c42=4&c43=0&c44=0';
		}
		if( $setRandomNo == 8 ){
			return 'score=0&c11=2&c12=0&c13=0&c14=0&c21=0&c22=2&c23=0&c24=4&c31=0&c32=0&c33=0&c34=0&c41=0&c42=0&c43=2&c44=0';
		}
		if( $setRandomNo == 9 ){
			return 'score=0&c11=0&c12=0&c13=0&c14=2&c21=0&c22=0&c23=0&c24=2&c31=0&c32=2&c33=0&c34=0&c41=0&c42=0&c43=0&c44=0';
		}
		if( $setRandomNo == 10 ){
			return 'score=0&c11=0&c12=0&c13=0&c14=0&c21=0&c22=0&c23=0&c24=0&c31=0&c32=0&c33=2&c34=2&c41=0&c42=0&c43=0&c44=0';
		}
	
	}
	
	function html_tile($tileid){
	$string = "";
	$tilevalue = $_REQUEST[$tileid];
	if($tilevalue==0){
		return $string;
	}else{
		if($tilevalue <= 2048){
			$string = '<div class="tile_'.$tilevalue.'"><br/>'.$tilevalue.'</div>';
			return $string;
		}
		else{
			$string = '<div class="tile_max"><br/>'.$tilevalue.'</div>';
			return $string;
		}
	}
}
/* findColumn : returns the GET values of tiles in a get string
(void) -> (String) */

function changeMove($v1,$v2,$v3,$v4){
	if( $v4 == 0 ){
		$v4=$v3;
		$v3=0;
	}
	if( $v3 == 0 ){
		$v3=$v2;
		$v2=0;
	}
	if( $v4 == 0 ){
		$v4=$v3;
		$v3=0;
	}
	if( $v2 == 0 ){
		$v2=$v1;
		$v1=0;
	}
	if( $v3 == 0 ){
		$v3=$v2;
		$v2=0;
	}
	if( $v4 == 0 ){
		$v4=$v3;
		$v3=0;
	}
	//Merge 3 and 4
	if( $v4 == $v3 ){
		$v4 = $v4 * 2 ;
		$v3 = 0 ;
	}
	if( $v3 == 0 ){
		$v3=$v2;
		$v2=0;
	}
	if( $v2 == 0 ){
		$v2=$v1;
		$v1=0;
	}
	//Merge 2 and 3
	if( $v3 == $v2 ){
		$v3 = $v3 * 2 ;
		$v2 = 0 ;
	}
	if( $v2 == 0 ){
		$v2=$v1;
		$v1=0;
	}
	//Merge 1 and 2
	if( $v2 == $v1 ){
		$v2 = $v2 * 2 ;
		$v1 = 0 ;
	}
	$ret = [ 1 => $v1 , 2 => $v2 , 3 => $v3 , 4 => $v4 ] ;
	return $ret ;
}

function movement($direction){//return $_REQUEST;
	$output = "";
	if($direction == 1){
		$columnVal1 = self::changeMove($_REQUEST["c41"],$_REQUEST["c31"],$_REQUEST["c21"],$_REQUEST["c11"]) ;
		$columnVal2 = self::changeMove($_REQUEST["c42"],$_REQUEST["c32"],$_REQUEST["c22"],$_REQUEST["c12"]) ;
		$columnVal3 = self::changeMove($_REQUEST["c43"],$_REQUEST["c33"],$_REQUEST["c23"],$_REQUEST["c13"]) ;
		$columnVal4 = self::changeMove($_REQUEST["c44"],$_REQUEST["c34"],$_REQUEST["c24"],$_REQUEST["c14"]) ;
		$result = "c11=".$columnVal1[4]."&c12=".$columnVal2[4]."&c13=".$columnVal3[4]."&c14=".$columnVal4[4]."&c21=".$columnVal1[3]."&c22=".$columnVal2[3]."&c23=".$columnVal3[3]."&c24=".$columnVal4[3]."&c31=".$columnVal1[2]."&c32=".$columnVal2[2]."&c33=".$columnVal3[2]."&c34=".$columnVal4[2]."&c41=".$columnVal1[1]."&c42=".$columnVal2[1]."&c43=".$columnVal3[1]."&c44=".$columnVal4[1] ;
		foreach(explode( '&', $result ) as $pair ) {
		list( $key, $result ) = explode( '=', $pair, 2 );
		$output[$key] = $result;
		}
		return $output;
		
	}
	if($direction == 2){
		$columnVal1 = self::changeMove($_REQUEST["c11"],$_REQUEST["c12"],$_REQUEST["c13"],$_REQUEST["c14"]) ;
		$columnVal2 = self::changeMove($_REQUEST["c21"],$_REQUEST["c22"],$_REQUEST["c23"],$_REQUEST["c24"]) ;
		$columnVal3 = self::changeMove($_REQUEST["c31"],$_REQUEST["c32"],$_REQUEST["c33"],$_REQUEST["c34"]) ;
		$columnVal4 = self::changeMove($_REQUEST["c41"],$_REQUEST["c42"],$_REQUEST["c43"],$_REQUEST["c44"]) ;
		$result = "c11=".$columnVal1[1]."&c12=".$columnVal1[2]."&c13=".$columnVal1[3]."&c14=".$columnVal1[4]."&c21=".$columnVal2[1]."&c22=".$columnVal2[2]."&c23=".$columnVal2[3]."&c24=".$columnVal2[4]."&c31=".$columnVal3[1]."&c32=".$columnVal3[2]."&c33=".$columnVal3[3]."&c34=".$columnVal3[4]."&c41=".$columnVal4[1]."&c42=".$columnVal4[2]."&c43=".$columnVal4[3]."&c44=".$columnVal4[4] ;
		foreach(explode( '&', $result ) as $pair ) {
		list( $key, $result ) = explode( '=', $pair, 2 );
		$output[$key] = $result;
		}
		return $output;
	}
	if($direction == 3){
		$columnVal1 = self::changeMove($_REQUEST["c11"],$_REQUEST["c21"],$_REQUEST["c31"],$_REQUEST["c41"]) ;
		$columnVal2 = self::changeMove($_REQUEST["c12"],$_REQUEST["c22"],$_REQUEST["c32"],$_REQUEST["c42"]) ;
		$columnVal3 = self::changeMove($_REQUEST["c13"],$_REQUEST["c23"],$_REQUEST["c33"],$_REQUEST["c43"]) ;
		$columnVal4 = self::changeMove($_REQUEST["c14"],$_REQUEST["c24"],$_REQUEST["c34"],$_REQUEST["c44"]) ;
		$result = "c11=".$columnVal1[1]."&c12=".$columnVal2[1]."&c13=".$columnVal3[1]."&c14=".$columnVal4[1]."&c21=".$columnVal1[2]."&c22=".$columnVal2[2]."&c23=".$columnVal3[2]."&c24=".$columnVal4[2]."&c31=".$columnVal1[3]."&c32=".$columnVal2[3]."&c33=".$columnVal3[3]."&c34=".$columnVal4[3]."&c41=".$columnVal1[4]."&c42=".$columnVal2[4]."&c43=".$columnVal3[4]."&c44=".$columnVal4[4] ;
		foreach(explode( '&', $result ) as $pair ) {
		list( $key, $result ) = explode( '=', $pair, 2 );
		$output[$key] = $result;
		}
		return $output;
	}
	if($direction == 4){
		$columnVal1 = self::changeMove($_REQUEST["c14"],$_REQUEST["c13"],$_REQUEST["c12"],$_REQUEST["c11"]) ;
		$columnVal2 = self::changeMove($_REQUEST["c24"],$_REQUEST["c23"],$_REQUEST["c22"],$_REQUEST["c21"]) ;
		$columnVal3 = self::changeMove($_REQUEST["c34"],$_REQUEST["c33"],$_REQUEST["c32"],$_REQUEST["c31"]) ;
		$columnVal4 = self::changeMove($_REQUEST["c44"],$_REQUEST["c43"],$_REQUEST["c42"],$_REQUEST["c41"]) ;
		$result = "c11=".$columnVal1[4]."&c12=".$columnVal1[3]."&c13=".$columnVal1[2]."&c14=".$columnVal1[1]."&c21=".$columnVal2[4]."&c22=".$columnVal2[3]."&c23=".$columnVal2[2]."&c24=".$columnVal2[1]."&c31=".$columnVal3[4]."&c32=".$columnVal3[3]."&c33=".$columnVal3[2]."&c34=".$columnVal3[1]."&c41=".$columnVal4[4]."&c42=".$columnVal4[3]."&c43=".$columnVal4[2]."&c44=".$columnVal4[1] ;
		foreach(explode( '&', $result ) as $pair ) {
		list( $key, $result ) = explode( '=', $pair, 2 );
		$output[$key] = $result;
		}
		return $output;
		//return $result ;
	}
	
	
	return $output;
}
/* addrandtile : generates a GET url with a new tile at a random location
(void) -> (string)*/
function addrandtile(){
	$test = self::hasvoid() ;
	while($test){
		$x = rand(1,4) ;
		$y = rand(1,4) ;
		if ($_REQUEST["c".$x.$y] == 0) {
			$newtilevalue = 2 * rand(1,2) ;
			$test = false;
			$returnurl = self::gentileget($newtilevalue,$x,$y,11)."&".self::gentileget($newtilevalue,$x,$y,12)."&".self::gentileget($newtilevalue,$x,$y,13)."&".self::gentileget($newtilevalue,$x,$y,14)."&" ;
			$returnurl .= self::gentileget($newtilevalue,$x,$y,21)."&".self::gentileget($newtilevalue,$x,$y,22)."&".self::gentileget($newtilevalue,$x,$y,23)."&".self::gentileget($newtilevalue,$x,$y,24)."&" ;
			$returnurl .= self::gentileget($newtilevalue,$x,$y,31)."&".self::gentileget($newtilevalue,$x,$y,32)."&".self::gentileget($newtilevalue,$x,$y,33)."&".self::gentileget($newtilevalue,$x,$y,34)."&" ;
			$returnurl .= self::gentileget($newtilevalue,$x,$y,41)."&".self::gentileget($newtilevalue,$x,$y,42)."&".self::gentileget($newtilevalue,$x,$y,43)."&".self::gentileget($newtilevalue,$x,$y,44) ;
			return $returnurl ;
		}
	}
	return ('gettile');
}
/* self::gentileget : Usual function for addrandtile() .
(int,int,int,int) -> (string)
*/
function gentileget ($tv,$sx,$sy,$tile_sid) {
	if("c".$sx.$sy == "c".$tile_sid){
				return "c".$tile_sid."=".$tv ;
			} else {
				return "c".$tile_sid."=".$_REQUEST["c".$tile_sid] ;
			}
}
/* canplay (predicate): returns if the user can play using curent GET values
(void) -> (boolean)*/
function canplay($getVal){ 

 //print_r($getVal);exit;
	for($for_1=1;$for_1<=4;$for_1++){
		for($for_2=1;$for_2<=4;$for_2++){
			if ($getVal["c".$for_1.$for_2] == 0){
				return true;
			}
		}
	}
	for($for_1=1;$for_1<=4;$for_1++){
		for($for_2=1;$for_2<=3;$for_2++){
			if($getVal["c".$for_1.$for_2] == $getVal["c".$for_1.($for_2 + 1)]){
				return true ;
			}
		}
	}
	for($for_1=1;$for_1<=3;$for_1++){
		for($for_2=1;$for_2<=4;$for_2++){
			if($getVal["c".$for_1.$for_2] == $getVal["c".($for_1 + 1).$for_2]){
				return true ;
			}
		}
	}
	return false;
}
/* haswon (predicate): returns if the user won using curent GET values
(void) -> (boolean)*/
function haswon(){
	for($for_1=1;$for_1<=4;$for_1++){
		for($for_2=1;$for_2<=4;$for_2++){
			if ($_REQUEST["c".$for_1.$for_2] >= 2048 ){
				return true;
			}
		}
	}
	return false;
}
/* self::hasvoid (predicate): returns if the current grid has at least 1 empty space.
(void) -> (boolean)*/
function hasvoid(){
	for($for_1=1;$for_1<=4;$for_1++){
		for($for_2=1;$for_2<=4;$for_2++){
			if ($_REQUEST["c".$for_1.$for_2] == 0){
				return true;
			}
		}
	}
	return false;
}

function getScore($direction){
	if($direction == 1){
		$columnVal1 = self::scoreValue($_REQUEST["c41"],$_REQUEST["c31"],$_REQUEST["c21"],$_REQUEST["c11"]) ;
		$columnVal2 = self::scoreValue($_REQUEST["c42"],$_REQUEST["c32"],$_REQUEST["c22"],$_REQUEST["c12"]) ;
		$columnVal3 = self::scoreValue($_REQUEST["c43"],$_REQUEST["c33"],$_REQUEST["c23"],$_REQUEST["c13"]) ;
		$columnVal4 = self::scoreValue($_REQUEST["c44"],$_REQUEST["c34"],$_REQUEST["c24"],$_REQUEST["c14"]) ;
		return $_REQUEST["score"] + $columnVal1 + $columnVal2 + $columnVal3 + $columnVal4 ;
	}
	if($direction == 2){
		$columnVal1 = self::scoreValue($_REQUEST["c11"],$_REQUEST["c12"],$_REQUEST["c13"],$_REQUEST["c14"]) ;
		$columnVal2 = self::scoreValue($_REQUEST["c21"],$_REQUEST["c22"],$_REQUEST["c23"],$_REQUEST["c24"]) ;
		$columnVal3 = self::scoreValue($_REQUEST["c31"],$_REQUEST["c32"],$_REQUEST["c33"],$_REQUEST["c34"]) ;
		$columnVal4 = self::scoreValue($_REQUEST["c41"],$_REQUEST["c42"],$_REQUEST["c43"],$_REQUEST["c44"]) ;
		return $_REQUEST["score"] + $columnVal1 + $columnVal2 + $columnVal3 + $columnVal4 ;
	}
	if($direction == 3){
		$columnVal1 = self::scoreValue($_REQUEST["c11"],$_REQUEST["c21"],$_REQUEST["c31"],$_REQUEST["c41"]) ;
		$columnVal2 = self::scoreValue($_REQUEST["c12"],$_REQUEST["c22"],$_REQUEST["c32"],$_REQUEST["c42"]) ;
		$columnVal3 = self::scoreValue($_REQUEST["c13"],$_REQUEST["c23"],$_REQUEST["c33"],$_REQUEST["c43"]) ;
		$columnVal4 = self::scoreValue($_REQUEST["c14"],$_REQUEST["c24"],$_REQUEST["c34"],$_REQUEST["c44"]) ;
		return $_REQUEST["score"] + $columnVal1 + $columnVal2 + $columnVal3 + $columnVal4 ;
	}
	if($direction == 4){
		$columnVal1 = self::scoreValue($_REQUEST["c14"],$_REQUEST["c13"],$_REQUEST["c12"],$_REQUEST["c11"]) ;
		$columnVal2 = self::scoreValue($_REQUEST["c24"],$_REQUEST["c23"],$_REQUEST["c22"],$_REQUEST["c21"]) ;
		$columnVal3 = self::scoreValue($_REQUEST["c34"],$_REQUEST["c33"],$_REQUEST["c32"],$_REQUEST["c31"]) ;
		$columnVal4 = self::scoreValue($_REQUEST["c44"],$_REQUEST["c43"],$_REQUEST["c42"],$_REQUEST["c41"]) ;
		return $_REQUEST["score"] + $columnVal1 + $columnVal2 	 + $columnVal3 + $columnVal4 ;
	}
	return 0;
}

function findColumn(){
	$string = "c11=".$_REQUEST["c11"]."&c12=".$_REQUEST["c12"]."&c13=".$_REQUEST["c13"]."&c14=".$_REQUEST["c14"]."&c21=".$_REQUEST["c21"]."&c22=".$_REQUEST["c22"]."&c23=".$_REQUEST["c23"]."&c24=".$_REQUEST["c24"]."&c31=".$_REQUEST["c31"]."&c32=".$_REQUEST["c32"]."&c33=".$_REQUEST["c33"]."&c34=".$_REQUEST["c34"]."&c41=".$_REQUEST["c41"]."&c42=".$_REQUEST["c42"]."&c43=".$_REQUEST["c43"]."&c44=".$_REQUEST["c44"]."" ;
	return $string;
}

function scoreValue($v1,$v2,$v3,$v4){
	$newlinescore = 0 ;
	if( $v4 == 0 ){
		$v4=$v3;
		$v3=0;
	}
	if( $v3 == 0 ){
		$v3=$v2;
		$v2=0;
	}
	if( $v4 == 0 ){
		$v4=$v3;
		$v3=0;
	}
	if( $v2 == 0 ){
		$v2=$v1;
		$v1=0;
	}
	if( $v3 == 0 ){
		$v3=$v2;
		$v2=0;
	}
	if( $v4 == 0 ){
		$v4=$v3;
		$v3=0;
	}
	//Merge 3 and 4
	if( $v4 == $v3 ){
		$v4 = $v4 * 2 ;
		$v3 = 0 ;
		$newlinescore = $newlinescore + $v4 ;
	}
	if( $v3 == 0 ){
		$v3=$v2;
		$v2=0;
	}
	if( $v2 == 0 ){
		$v2=$v1;
		$v1=0;
	}
	//Merge 2 and 3
	if( $v3 == $v2 ){
		$v3 = $v3 * 2 ;
		$v2 = 0 ;
		$newlinescore = $newlinescore + $v3 ;
	}
	if( $v2 == 0 ){
		$v2=$v1;
		$v1=0;
	}
	//Merge 1 and 2
	if( $v2 == $v1 ){
		$v2 = $v2 * 2 ;
		$v1 = 0 ;
		$newlinescore = $newlinescore + $v2 ;
	}
	return $newlinescore ;
}

}
?>