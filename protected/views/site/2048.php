<?php
if(!isset($_REQUEST["score"])){
	$value=gameComponent::setRandomNo();
	foreach ( explode( '&', $value ) as $pair ) {
		list( $key, $value ) = explode( '=', $pair, 2 );
		$output[$key] = $value;
	} 
	$this->redirect(Yii::app()->createUrl('site/2048',$output));
	exit();
}
/*Spawns a tile if the user moved*/
if(isset($_REQUEST["move"])){
		if(gameComponent::addrandtile()=='gettile'){
			$value=gameComponent::findColumn();
			foreach ( explode( '&', $value ) as $pair ) {
				list( $key, $value ) = explode( '=', $pair, 2 );
				$output[$key] = $value;
			} 
			$output['score']=$_REQUEST["score"];
		}else{
			$val=gameComponent::addrandtile();
			foreach ( explode( '&', $val ) as $pair ) {
				list( $key, $val ) = explode( '=', $pair, 2 );
				$output[$key] = $val;
			} 
			$output['score']=$_REQUEST["score"];
		}
		$this->redirect(Yii::app()->createUrl('site/2048',$output)) ;
	exit();
}
/*Takes you to the end page if you can't play.*/
if( ! isset($_REQUEST["page"]) || $_REQUEST["page"] == 0){
	if( !gameComponent::canplay($_REQUEST) ) {
		$this->redirect(Yii::app()->createUrl('site/2048', array('score'=>$_REQUEST["score"],'page'=>'1'))) ;
		exit() ;
	}	
}

if(isset($_REQUEST["page"])){ 
	$page = $_REQUEST["page"] ;
} else {
	$page = 0 ;
}

?>
<!Doctype html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>
		My 2048 For You
	</title>
	
</head>
<body>
<div class="page">
		<?php
		if($page == 0) {
		$tiles=gameComponent::findColumn();
		foreach ( explode( '&', $tiles ) as $pair ) {
			list( $key, $tiles ) = explode( '=', $pair, 2 );
			$output[$key] = $tiles;
		} 
		$output['score']=$_REQUEST["score"];
		$output['page']=2;
		?><br>
		<header class="sub">
			<br/>
			<span class="title">
				<strong>
					2048
				</strong>
			</span>
			<span class="score">
				score :
				<?php echo($_REQUEST["score"]) ; ?>
			</span>
			<span class="new">
				<a href="<?php echo (Yii::app()->createUrl('site/2048'));?>">New game</a>
			</span>
		</header>
		<br>
		<div class="grid">
			<table>
				<tr>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c11")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c12")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c13")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c14")) ;?></th>
				</tr>
				<tr>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c21")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c22")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c23")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c24")) ;?></th>
				</tr>
				<tr>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c31")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c32")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c33")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c34")) ;?></th>
				</tr>
				<tr>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c41")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c42")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c43")) ;?></th>
					<th class="gridhead"><?php echo(gameComponent::html_tile("c44")) ;?></th>
				</tr>
			</table>
		</div>
		<div class="dpad">
			<table>
			<tr>
				<th></th>
				<th class="key"><a href="<?php 
				if($key1=gameComponent::movement(1)){
					$key1['score']=gameComponent::getScore(1);
					$key1['move']='1';
				}
				echo Yii::app()->createUrl('site/2048', $key1);
				?>">1</a></th>
				<th></th>
			</tr>
			<tr>
				<th class="key"><a href="<?php 
				if($key4=gameComponent::movement(4)){
					$key4['score']=gameComponent::getScore(4);
					$key4['move']='1';
				}
				echo Yii::app()->createUrl('site/2048', $key4); ?>">2</a></th>
				<th></th>
				<th class="key"><a href="<?php 
				if($key2=gameComponent::movement(2)){
					$key2['score']=gameComponent::getScore(2);
					$key2['move']='1';
				}
				echo Yii::app()->createUrl('site/2048', $key2) ; ?>">3</a></th>
			</tr>
			<tr>
				<th></th>
				<th class="key"><a href="<?php
				if($key3=gameComponent::movement(3)){
					$key3['score']=gameComponent::getScore(3);
					$key3['move']='1';
				}
				echo Yii::app()->createUrl('site/2048', $key3) ; ?>">4</a></th>
				<th></th>
			</tr>
			</table>
		</div>
		<br/>
			<?php
			if(gameComponent::haswon()){
			?>
				You win the Game!!
			<?php
			}
			if( $_REQUEST["score"] > 300000 ){
			?><h1>Heeeey your score is high!!</h1>			
			<?php
			}
			?>
		<br/>
		<?php
		}
		if($page == 1){
		?>
			<span class="title">
				<h1>
					2048 Game!!
				</h1>
			</span>
			<span class="score">
				score :
				<?php echo($_REQUEST["score"]) ; ?>
			</span>
			<span class="new">
				<a href="<?php echo (Yii::app()->createUrl('site/2048'));?>">New game</a>
			</span>	
		<h1>
			Game Over
		</h1>
		<?php
		}
		if($page == 2){
		?>
		<header class="sub">
			<span class="title">
				<h1>2048 Game</h1>
			</span>
			<span class="score">
				score :
				<?php echo($_REQUEST["score"]) ; ?>
			</span>
			<span class="new">
				<a href="<?php
				$value=gameComponent::findColumn();
				foreach ( explode( '&', $value ) as $pair ) {
					list( $key, $value ) = explode( '=', $pair, 2 );
					$output[$key] = $value;
				} 
				$output['score']=$_REQUEST["score"];
				echo Yii::app()->createUrl('site/2048', $output); ?>">Click to New Game!!/a>
			</span>
		</header>
		<?php
		}
		?>
	</div>			
</body>
</html>